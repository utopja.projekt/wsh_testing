import requests
from faker import Faker

import endpoints

fake = Faker()


class Account:
    def __init__(self):
        self.name = fake.uuid4()

    def create(self):
        body = {"name": self.name}
        create_account_response = requests.put(endpoints.accounts_create, json=body)
        assert create_account_response.status_code == 201

    def delete(self):
        params = {"account": self.name}
        del_account = requests.delete(endpoints.accounts_delete, params=params)
        assert del_account.status_code == 200

    def pay(self, amount):
        account_params = {"account": self.name}
        body = {'amount': amount}
        pay_response = requests.post(endpoints.accounts_pay, json=body, params=account_params)
        assert pay_response.status_code == 200

    def withdraw(self, amount):
        account_params = {"account": self.name}
        body = {'amount': amount}
        withdraw_response = requests.post(endpoints.accounts_withdraw, json=body, params=account_params)
        assert withdraw_response.status_code == 200

    def get_balance(self):
        list_params = {"account": self.name}
        response = requests.get(endpoints.accounts, params=list_params)
        return response.json()['accounts'][0]['balance']['accountBalance']

if __name__ == '__main__':
    account = Account()
    account.create()
    print(account.name)
    account.delete()

