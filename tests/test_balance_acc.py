import requests
import pytest
import endpoints

from faker import Faker

fake = Faker()


def test_balance_accounts(account_name):
    list_params = {"account": account_name}
    response = requests.get(endpoints.accounts, params=list_params)
    response_dict = response.json()
    balance = response_dict['accounts'][0]['balance']['accountBalance']
    print(balance)
    assert balance == 1000






@pytest.fixture
def account_name():
    random_name = fake.uuid4()
    body = {
        "name": random_name
    }
    create_account_response = requests.put(endpoints.accounts_create, json=body)
    assert create_account_response.status_code == 201
    return random_name