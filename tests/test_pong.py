import requests
import endpoints


def test_pong():
    ping_response = requests.get(endpoints.ping)
    # print(ping_response.text)
    assert ping_response.status_code == 200
    assert ping_response.text == 'pong'


def test_ping_As_json():
    headers_dict = {
        'Accept': 'application/json'
    }
    ping_response = requests.get(endpoints.ping, headers=headers_dict)
    response_dict = (ping_response.json())
    assert response_dict['reply'] == 'pong!'
    # assert ping_response.status_code == 200
    # assert ping_response.json == 'pong'
