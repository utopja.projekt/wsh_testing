import pytest
import requests
from faker import Faker

from models.account import Account

fake = Faker()
import endpoints


def test_get_account_list(new_account):
    response = requests.get(endpoints.accounts)
    assert response.status_code == 200

    response_dict = response.json()
    # print(response.json())
    # print(response_dict['accounts'])
    accounts_list = response_dict['accounts']
    names_list = [a['name'] for a in accounts_list]
    # print(names_list)

    assert new_account.name in names_list


def test_create_account(account_name):
    list_params = {'account': account_name}
    filter_list_response = requests.get(endpoints.accounts, params=list_params)
    assert filter_list_response.status_code == 200
    assert account_name in filter_list_response.text


def test_del_account(new_account):
    new_account.delete()

    list_params = {"account": new_account.name}

    del_response = requests.get(endpoints.accounts, params=list_params)
    assert del_response.status_code == 404


# def _create_accounts():
#     random_name = fake.uuid4()
#     body = {
#         "name": random_name
#     }
#     create_account_response = requests.put(endpoints.accounts_create, json=body)
#     assert create_account_response.status_code == 201
#     return random_name

def test_balance_accounts(new_account):
    assert new_account.get_balance() == 1000


def test_balance(new_account):
    new_account.pay(200)
    assert new_account.get_balance() == 1200
    new_account.withdraw(133)
    assert new_account.get_balance() == 1067


@pytest.fixture
def new_account():
    account = Account()
    account.create()
    return account
