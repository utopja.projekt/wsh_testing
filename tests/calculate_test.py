import requests
import random
import endpoints

first_number, second_number = random.randint(0, 1000), random.randint(0, 1000)

def test_calc_add():
    body_add = {
        "firstNumber": first_number,
        "secondNumber": second_number
    }
    add_number = requests.post(endpoints.calculator_add, json=body_add)
    print(add_number.json())
    assert add_number.status_code == 200

    response_dict = add_number.json()
    actual_result = response_dict['result']
    assert actual_result == first_number + second_number
